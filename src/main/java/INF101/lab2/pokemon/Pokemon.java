package INF101.lab2.pokemon;

import java.lang.Math;
import java.lang.reflect.Constructor;
import java.util.Random;

public class Pokemon implements IPokemon {

    
    // Oppgave 1 deloppgave A: opprett feltvariabler
    public String name;
    public int healthPoints;
    public int maxHealthPoints;
    public int strength;
    public Random random;

    // Konstruktør.  Initierer feltvariablene
    public Pokemon(String name) {
        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
    }

    public String getName() {
        return name;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getCurrentHP() {
        return healthPoints;
    }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;
    }

    public boolean isAlive() {
        if (getCurrentHP() > 0){
            return true;
        }
        else {
            return false;
        }
    }

    //Oppgave 5
    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian()); //regner ut hvor mange health points som skal tas fra target
        System.out.println(this.getName() + " attacks " + target.getName() + ".");
        target.damage(damageInflicted); //Sier at damage metoden skal anvendes på target objektet
        
        if (! target.isAlive()){ //anvender isAlive metoden på target objektet
            System.out.println(target.getName() + " is defeated by " + this.name + ".");
        }

    }

    //Oppgave 4
    @Override
    public void damage(int damageTaken) {
        if (damageTaken >= 0) {
            if (damageTaken > healthPoints) {
                healthPoints = 0;
            }
            else {
                healthPoints -= damageTaken;
            }
        }
        System.out.println(name + " takes " + damageTaken + " damage and is left with " + healthPoints + "/" + maxHealthPoints + " HP");
    }

    // Oppgave 3
    @Override
    public String toString() {
        String pokemonStr = (name + " HP: (" + healthPoints + "/"+ maxHealthPoints + ") STR: " +strength);
        return pokemonStr;
    }

}
